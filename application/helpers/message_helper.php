<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('send_message')) {
    function send_message($mobile, $message, $unicode = false)
    {
        if(!defined('SMS_API_USERNAME')) return false;
        $sender = 'WBCN';
        $url = "http://bulksms.com.pk/api/sms.php?username=" . SMS_API_USERNAME . "&password=" . SMS_API_PASSWORD . "&sender=" . urlencode($sender) . "&mobile=" . urlencode($mobile) . "&message=" . urlencode($message);
        if ($unicode) $url .= 'type=unicode';

        //Curl start
        $ch = curl_init();
        $timeout = 30;
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

        $response = curl_exec($ch);
        if (curl_errno($ch))
            print curl_error($ch);
        else
            curl_close($ch);
        $res = explode(' ', $response);
        return $res;
    }
}

if (!function_exists('message_balance')) {
    function message_balance()
    {
        if(!defined('SMS_API_USERNAME')) return false;
        $url = "https://sendpk.com/api/balance.php?username=".SMS_API_USERNAME."&password=".SMS_API_PASSWORD."";
        $ch  =  curl_init();
        $timeout  =  30;
        curl_setopt ($ch,CURLOPT_URL, $url) ;
        curl_setopt ($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch,CURLOPT_CONNECTTIMEOUT, $timeout) ;
        $response = curl_exec($ch) ;
        curl_close($ch) ;

        return $response ;
    }
}