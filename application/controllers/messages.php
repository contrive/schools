<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends Base_Controller{

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata(SESSION_CONST_PRE.'userId'))
		{
			redirect('login', 'refresh');
		}
		$this->load->model('Branchs_model', '', TRUE);
		$this->load->helper('message');
	}
	
	function index(){
		
		$this->branchs_list();
	}
	
	function send_msg(){
		if(isset($_POST['course_id'])){
			$course_id = $_POST['course_id'];
			$section = $_POST['section'];
			$sender = "FAST Notification" ;
			
			if($_POST['course_id'] == -1 && isset($_POST['cell_number']) && strlen($_POST['cell_number']) == 12 ){
				
				$mobile = $_POST['cell_number'];
				$message = $_POST['message_text'];

                $sms_flag = send_message($mobile, $message);
                Base_model::insert_message_log($course_id, $mobile, $message, 1);
                echo $sms_flag[0]. '<br/> Successfully sent messages(1).';

			} elseif ( $_POST['course_id'] == 0 ) {
				
				$student_arr = Base_model::get_student_by_class_section($course_id, $section);
				$student_list = ''; $count = 0;
				foreach ($student_arr as $res)
				{
					if( isset($res->cell_phone_father)  && strlen($res->cell_phone_father) == 12 ){
						$count++;
						$cell = $res->cell_phone_father;
						$student_list .= ','.$cell;
					}
				}

				$mobile = substr($student_list, 1, strlen($student_list));
				$message = $_POST['message_text'];
                $sms_flag = send_message($cell, $message);
				Base_model::insert_message_log($course_id, $mobile, $message, $count);
                echo $sms_flag[0]. '<br/> Successfully sent messages ['.$count. ']';

			} elseif ( $_POST['course_id'] > 0 ) {
				$student_arr = Base_model::get_student_by_class_section($course_id, $section, $_POST['cdel']);
//				var_dump($student_arr); die;
				$student_list = ''; $count = 0;
				foreach ($student_arr as $res)
				{
					if( isset($res->cell_phone_father)  && strlen($res->cell_phone_father) == 12 ){
						$count++;
						$cell = $res->cell_phone_father;
						$student_list .= ','.$cell;
					}
				}
				$mobile = substr($student_list, 1, strlen($student_list));
				$message = $_POST['message_text'];
				
				$sms_flag = send_message($mobile, $message);
				Base_model::insert_message_log($course_id, $mobile, $message, $count, $sms_flag[0]);
				echo $sms_flag[0]. '<br/> Successfully sent ('.$count. ') messages.';
			}
		}
		else{
			redirect('logout','location');			
		}
	}

	function branchs_list()
    {
		$this->data['courses_list'] = Base_model::get_all_courses();
		$this->data['company_list'] = null; //Base_model::getCompanyList();
		$this->data['message_log_list'] = $this->Branchs_model->get_message_log();
		$this->data['branchs_list'] = $this->Branchs_model->get_all_branchs();
		$this->load_template('settings/messages/default');
	}
	
	function save_item(){
		if($this->Branchs_model->process_already_run()){
			$this->Branchs_model->insert();	
		}
		$this->data['branchs_list'] = $this->Branchs_model->get_all_branchs();
		$this->load->view('settings/branchs/list', $this->data);	
	}
	
	function add(){
		redirect('/messages','location');
	}
	function edit(){
		$this->branchs_list();
	}
}