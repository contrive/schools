<?php


class SettingModel extends CI_Model
{
    public function find($id) {

        $this->db->where('id', $id);
        $query = $this->db->get('settings');
        return $query->result();
    }

    public function getAll() {

        $this->db->where('deleted_at', NULL);
        $query = $this->db->get('settings');
        return $query->result();
    }

    public function updateSettingKey($key, $value)
    {
        return $this->db->update('settings',
            array('setting_value' => $value),
            array('setting_key' => $key)
        );
    }

}