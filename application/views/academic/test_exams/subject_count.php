<div class="row">
<form id="marksheetForm11" action="<?= base_url(). $model.'/student_count_per_subject'?>" method="post" >
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
<?php
	  $c = isset($_POST['course_id']) ? $_POST['course_id'] : $this->session->userdata(SESSION_CONST_PRE.'course_id');
	  echo '<select name="course_id" class="form-control col-md-6  col-sm-6" id="course_id" style="width: 150px" onchange="marksheetForm11.submit();" >';
			foreach($courses_list as $course){
				$course_id = $course->course_id;
				$sel = ($course_id ==  $c) ?  'selected' : '' ;
				echo '<option value="'.$course_id.'" '.$sel.'>'.$course->course_name.'</option>';
			}
	  echo '</select>';
?>
	</div>
	<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6"></div>
</form>
</div>
<br />
<div class="row">
<?php $i = 1;?>	
<?php  foreach($subuject_count as $subject){ $color = ($i % 2 == 0) ? 'blue' : 'green';?>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
		<div class="dashboard-stat <?= $color ?>">
			<div class="visual">
				<i class="fa fa-sign-in"></i>
			</div>
			<div class="details">
				<div class="number">
					<?php echo $subject -> total_student_per_subject;?>
				</div>
				<div class="desc">                           
					<?php echo $subject -> subject_name;?>
				</div>
			</div>
			<a class="more" href="<?php echo base_url();?>students">
			<?php echo Base_Controller::ToggleLang('View more');?> <i class="m-icon-swapright m-icon-white"></i>
			</a>
		</div>
	</div>
<?php $i++; } ?>	
</div>	