<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require_once __DIR__ . '/autoload.php';

use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\Printer;


class Mike42 {

  private $CI;
  private $connector;
  private $printer;

  // TODO: printer settings
  // Make this configurable by printer (32 or 48 probably)
  private $printer_width = 32;

  function __construct()
  {
    $this->CI =& get_instance(); 
  }

  function connect($ip_address, $port)
  {
    $this->connector = new NetworkPrintConnector($ip_address, $port);
    $this->printer = new Printer($this->connector);
	
	echo 'connect call 2';
  }

  private function check_connection()
  {
    if (!$this->connector OR !$this->printer OR !is_a($this->printer, 'Mike42\Escpos\Printer')) {
      throw new Exception("Tried to create receipt without being connected to a printer.");
    }
  }

  public function close_after_exception()
  {
    if (isset($this->printer) && is_a($this->printer, 'Mike42\Escpos\Printer')) {
      $this->printer->close();
    }
    $this->connector = null;
    $this->printer = null;
    $this->emc_printer = null;
  }

  // Calls printer->text and adds new line
  private function add_line($text = "", $should_wordwrap = true)
  {
    $text = $should_wordwrap ? wordwrap($text, $this->printer_width) : $text;
    $this->printer->text($text."\n");
  }


  public function print_test_receipt($text = "")
  {

    $this->check_connection();
    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->add_line("TESTING");
    $this->add_line("Receipt Print");
    $this->printer->selectPrintMode();
    $this->add_line(); // blank line
    $this->add_line($text);
    $this->add_line(); // blank line
    $this->add_line(date('Y-m-d H:i:s'));
    $this->printer->cut(Printer::CUT_PARTIAL);
    $this->printer->close();
  }
  
  public function print_receipt($arr, $header)
  {
	$arr_order_type = array('', 'Take Away', 'Dine in', 'Delivery');
    $this->check_connection();
    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->add_line("".$arr_order_type[$header->order_type]);
	$this->add_line("Request #: ".$header->request_number);
	$this->add_line("Receipt #: ".$header->reference_number);
	$this->add_line(date('Y-m-d H:i:s'));
    $this->printer->selectPrintMode();
    $this->add_line(); // blank line
	foreach($arr as $val){
		$this->add_line($val->name. '   -   ' . $val->item_quantity);
		$this->add_line(); // blank line	
	}
    $this->printer->cut(Printer::CUT_PARTIAL);
    $this->printer->close();
  }

}