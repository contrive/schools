<?php
class Base_Model extends CI_Model {
	
	var $ref = NULL;
	var $formType;
	var $subType = NULL;
	var $formUrl;
	var $status;
	var $batch_number = '';
	
	var $subject = NULL;
	var $priority = NULL;
	var $urgency = NULL;
	var $owner_id = 0; 
	var $formReadOnlyUrl = NULL;
	var $cur_position;
	var $level = -1;
	var $h;
	var $record_per_page = 10;
	
	function __construct()
    {
        parent::__construct();
        $this->set_timezone();
    }
	
    function insert(){}
	
    function update(){}
	
    function delete(){}
	
    public function recordUserActionLog($user_id, $controller_name, $is_default){
    	$query = "INSERT INTO useractionlogs (user_id, action_desc, client_ip, path, action_loc) VALUES ('$user_id', '$controller_name', '".$_SERVER['REMOTE_ADDR']."', '".$_SERVER['REQUEST_URI']."','$is_default')";
    	$this->db->query($query);
    }
    
	public function set_timezone(){
       $this->db->query("SET time_zone='+3:00'");
           //$this->db->query("INSERT into userrole(user_id,role_id) values (SELECT EMP_NUM, 3 from emp_basic_info )");
    }
    
	function getCurrentConference(){
		
	}
	
	function get_today_conference(){
		
	}
	
	function get_all_branches(){
		$query = $this->db->get('branchs');
		$result = $query->result();
		return $result;	
	}

	function get_all_conference(){
	
	}
	
	function getUsersByRole($role_id, $department)
	{
		$query = "SELECT users.user_id, users.name from users join userrole on users.user_id = userrole.user_id and role_id = $role_id where users.department_id = '".$department."' and users.branch_id = ".$this->session->userdata(SESSION_CONST_PRE.'branch_id');
		
		$query = $this->db->query($query);
		$result = $query->result();
		return $result;	
	}
		
	function getPlacesList(){
		$query = "select * from places order by name";		
		$query = $this->db->query($query);
		return $query->result();
	}

	function get_department_attribute($id){
		$query = "select search_fields from departments where department_id=$id";
		$query = $this->db->query($query);
		$rs = $query->result();
		$sf = '';
		if(isset($rs[0])){
			$sf = $rs[0]->search_fields;
		}
		return $sf;
	}
	
	function get_document_list($doc_type){
		$this->db->select('d.*, departments.name as department_name, users.name as uploaded_by');
		$this->db->from('documents d');
		$this->db->join('departments','departments.department_id = d.department_id', 'LEFT');
		$this->db->join('users','users.user_id = d.user_id', 'LEFT');
		$this->db->where('document_type', $doc_type);
		$this->db->order_by('created_date', 'desc')->limit(10);
		//$query = "select * from documents where document_type='$doc_type' order by created_date desc limit 10";
		$query = $this->db->get();
		$rs = $query->result();
		
		return $rs;
	}
	
	function get_mosque_list(){
		$this->db->select('m.*, c.name as city_name');
		$this->db->from('ws_masjids m');
		$this->db->join('ws_cities c','m.city = c.city_id');
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_event_list(){
		$query = $this->db->get('events');
		
		return $query->result();
	}
	
	function get_cities($proId=6){
		$this->db->where('province_id', $proId);
		$query = $this->db->get('ws_cities');
		
		return $query->result();
	}
	
	function getAllUsers($assign_list, $role)
	{
		if(empty($assign_list)){
			$assign_list = '0';
		}	
		$where_clause = " and users.user_id in ($assign_list)";
		
		
		$query = "SELECT users.user_id, users.name from users join userrole on users.user_id = userrole.user_id and role_id = $role $where_clause";
		$query = $this->db->query($query);
		return $query->result();
	}
	
	function getTechnicianIT()
	{
		$query = "SELECT users.user_id, users.name from users join userrole on users.user_id = userrole.user_id and role_id = 5 and users.department_id = '110'";
		$query = $this->db->query($query);
		return $query->result();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $screen
	 * @param unknown_type $role
	 */
	
	function authenticateScreen($screen, $role){
		$q 	= "	select screens.screen_id from screens 
				join rolescreen on screens.screen_id = rolescreen.screen_id 
				where screens.url = ? 
				and rolescreen.role_id =  ?";
		$query = $this->db->query($q,array($screen, $role));
		
		if ($query->num_rows() > 0) {
			$row = $query->row();
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	function getAuthorizedActions($user_id, $screen){
		$q = " select sa.action_id, a.name from screen_actions sa join screens s on  sa.screen_id = s.screen_id
				join actions a on  a.action_id = sa.action_id
				where sa.user_id = ? and s.url=?";
		$query = $this->db->query($q,array($user_id, $screen));
		return $query->result();
	}

	function getLables(){
		$query = $this->db->get('localize');
		return $query->result();
	}
		
	function get_country_list(){
		$this->db->order_by('country_name');
		$query = $this->db->get('country');
		
		return $query->result();
	}
	
	function get_all_users($list = NULL, $not_in=FALSE){
		if(!is_null($list)){
			$list = explode(',', $list);
			if ($not_in){
				$this->db->where_not_in('user_id', $list);
			}
			else
			{
				$this->db->where_in('user_id', $list);	
			}
		}
		$query = $this->db->get('users');
		return $query->result();
	}
	
	function get_all_salemen(){
		$this->db->where('default_role =','10');
		$query = $this->db->get('users');
		return $query->result();
	}
	
	function get_all_units(){
		$query = $this->db->get('units');
		return $query->result();
	}
	
	public function get_customers($name=''){
	
		if($name != '')
		$this->db->like('name', $name);
		$query = $this->db->get('customers');
	
		return $query->result();
	}
	
	function get_all_location(){
		$query = $this->db->get('locations');
		return $query->result();
	}

	function get_notified_users($notified = TRUE){
		if($notified)
		$this->db->where('contract_expiry_notification', '1');
		else
		$this->db->where('contract_expiry_notification', '0');
		$query = $this->db->get('users');
		return $query->result();
	}
	
	function get_supervisor_users(){
		$query = $this->db->query("SELECT users.user_id, users.name from users join userrole on users.user_id = userrole.user_id and role_id = 6");
		//$query = $this->db->get('users');
		return $query->result();
	}
	
	function get_userrole($id){
		$query = $this->db->get_where('userrole',array("user_id" => $id));
		return $query->row();
	}
	
	public function get_employees(){
		$query = $this->db->query("select * from employees order by iqama_expiry asc limit 0,10");
		return $query->result();
	}
	
	function get_all_departments($row = 0, $paging = FALSE){
		if($paging){
			$query = $this->db->get('departments',10,$row);
		}
		else
		{
			$query = $this->db->get('departments');
		}
		return $query->result();
	}
	
	/***
	 * HR / School System function Definations From here
	*/
	
	function get_all_employees($terms = null ){
		if(!is_null($terms)){
			$sql = "SELECT *
			FROM employees
			WHERE employee_name LIKE '$terms%' ";
			if(is_numeric($terms)){
			$sql = "SELECT *
			FROM employees
				WHERE employee_id = '$terms' ";
			}
			$query = $this->db->query($sql);
			}
			else
			{
			$query = $this->db->get('employees');
		}
		return $query->result();
	}
	
	/***
	 * School System function Definations From here
	 */
	function get_students_by_batch($id){
		$this->db->where('batch_id', $id);
		$this->db->order_by('student_name');
		$query = $this->db->get('students');
	
		return $query->result();
	}
	
	function get_student_info($id){
		$this->db->where('student_id', $id);
		$query = $this->db->get('students');
	
		return $query->result();
	}
	
	function get_student_batch_course($id){
		$this->db->select('s.*, b.batch_name, c.course_name');
		$this->db->from('students s');
		$this->db->join('courses c','s.course_id = c.course_id', 'LEFT');
		$this->db->join('batches b','s.batch_id = b.batch_id', 'LEFT');
		$this->db->where('s.student_id', $id);
		$query = $this->db->get();
	
		return $query->result();
	}
	
	function get_student_assesments($id){
		$this->db->select('ess.*, e.exam_name, ed.max_marks, ed.min_marks, s.subject_name');
		$this->db->from('exam_score_subject ess');
		$this->db->join('exams e','ess.exam_id = e.id');
		$this->db->join('exam_details ed','ed.id = ess.exam_detail_id');
		$this->db->join('subjects s','ed.subject_id = s.subject_id');
		$this->db->where('ess.student_id', $id);
		$this->db->order_by('e.exam_name');
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function get_grading_levels_by_batch($id){
		$this->db->where('batch_id', $id);
		$this->db->order_by('min_scores', 'asc');
		$query = $this->db->get('batch_grading_level');
		return $query->result();
	}
	
	function get_collection_info($id){
		$this->db->where('id', $id);
		$query = $this->db->get('fee_collection');
	
		return $query->result();
	}
	
	function get_all_batches($course_id=NULL){
		if(isset($course_id) && !is_null($course_id)){
			$sql = "SELECT * FROM batches
			WHERE course_id = $course_id ";
			//$this->db->order_by('batch_name');
			$query = $this->db->query($sql);
		}
		else
		{
			$this->db->order_by('batch_name');
			$query = $this->db->get('batches');
		}	//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_weekdays($id){
		$sql = "SELECT * FROM batch_weekdays
		WHERE batch_id = $id ";
	
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function get_all_courses(){
		$this->db->order_by('course_name');
		$query = $this->db->get('courses');
	
		return $query->result();
	}
	
	function get_fee_categories(){
		$this->db->select('fc.id, b.batch_name, fc.category_name');
		$this->db->from('fee_category fc');
		$this->db->join('batches b','fc.batch_id = b.batch_id');
		$this->db->order_by('b.batch_name,  fc.category_name');
		$query = $this->db->get();
	
		return $query->result();
	}
	
	
	function get_subjects_by_course($id){
		$this->db->select('cs.*, s.subject_name, s.subject_code');
		$this->db->from('course_subject cs');
		$this->db->join('subjects s','cs.subject_id = s.subject_id');
		$this->db->where('cs.course_id', $id);
		$this->db->order_by('s.subject_name');
		$query = $this->db->get();
	
		return $query->result();
	}
	
	function get_employee_by_subject($id){
		$this->db->select('se.*, e.employee_name');
		$this->db->from('subject_employee se');
		$this->db->join('employees e','se.employee_id = e.employee_id');
		$this->db->where('se.subject_id', $id);
		$this->db->order_by('e.employee_name');
		$query = $this->db->get();
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_all_subjects($row = 0, $paging = FALSE){
	
		if($paging){
			$query = $this->db->get('subjects',10,$row);
		}
		else
		{
			$query = $this->db->get('subjects');
		}//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	public function get_adminition_no(){
		$query = $this->db->query("select max(student_id) as id from students");
		$result = $query->result();
		$max_num = $result[0]->id;
		return $max_num+1;
			
	}
	
	public function get_guardians($student_id){
		$query = $this->db->get_where('student_guardian',array("student_id" => $student_id, 'default'=>'1'));
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	public function get_previous_data($id, $is_student = true){
		if($is_student){
			$query = $this->db->get_where('student_previous_data',array("student_id" => $id));
		}
		else{
			$query = $this->db->get_where('student_previous_data',array("id" => $id));
		}
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_date_pre($dt){
		$sql = "select DATE_SUB('$dt', INTERVAL 1 MONTH) as dt";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->dt;
	}
	
	function get_date_next($dt){
		$sql = "select DATE_ADD('$dt', INTERVAL 1 MONTH) as dt";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->dt;
	}
	 
	function get_month_name($dt){
		$sql = "select MONTHNAME('$dt') as month_name";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->month_name;
	}
	
	/***
	 * School System function Definations End @ 29-04-2014
	*/
	
	/***
	 * Function define for Meeting Management Block
	 */
	
	function get_meeting_rooms(){
		//$this->db->where(array("is_active" => 'Y'));
		$query = $this->db->get('meeting_rooms');
		$result = $query->result();
		return $result;
	}
	
	/***
	 * End Meeting Management Block
	 */
	
	/***
	 * Function define for Easy Business Management Block
	*/
	function set_status_level($id, $level, $status){
		$sql = "Update rfq SET level = concat('$level',level), status='$status' where rfq_id = $id ";
		$query = $this->db->query($sql);
	}
	
	/***
	 * End Easy Business Management Block
	*/
	
	/***
	 * HR and Payroll Block
	*/ 
	
	public function get_employee_designation(){
		$query = $this->db->query("select distinct designation from employees order by designation asc");
		return $query->result();
	}
	
	/***
	 * End HR and Payroll Block
	*/
	
	/***
	 * CV DATABASE Jobs portal
	 * 
	 * BY Imran Saleem
	 * 26-10-2014 4:23 pm
	 */
	
	function get_jobs_ages(){
		$this->db->where(array("status" => '1'));
		$this->db->order_by('ordering');
		$query = $this->db->get('job_ages');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_jobs_careerlevels(){
		$this->db->where(array("status" => '1'));
		$this->db->order_by('ordering');
		$query = $this->db->get('job_careerlevels');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_jobs_shifts(){
		$this->db->where(array("isactive" => '1'));
		$this->db->order_by('ordering');
		$query = $this->db->get('job_shifts');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_jobs_categories(){
		$this->db->where(array("isactive" => '1'));
		$this->db->order_by('ordering');
		$query = $this->db->get('job_categories');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_jobs_subcategories($categoryid){
		$this->db->where(array("status" => '1', "categoryid"=>$categoryid));
		$this->db->order_by('ordering');
		$query = $this->db->get('job_subcategories');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_jobs_jobtypes(){
		$this->db->where(array("isactive" => '1'));
		$this->db->order_by('ordering');
		$query = $this->db->get('job_jobtypes');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_jobs_jobstatus(){
		$this->db->where(array("isactive" => '1'));
		$this->db->order_by('ordering');
		$query = $this->db->get('job_jobstatus');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_jobs_jobcities(){
		$this->db->where(array("enabled" => '1'));
		$this->db->order_by('name');
		$query = $this->db->get('job_cities');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_jobs_companies(){
		$this->db->where(array("status" => '1'));
		$this->db->order_by('name');
		$query = $this->db->get('job_companies');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_jobs_departments($companyid){
		$this->db->where(array("status" => '1', "companyid"=>$companyid));
		$this->db->order_by('name');
		$query = $this->db->get('job_departments');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_jobs_cities(){
		$this->db->where(array("enabled" => '1'));
		$this->db->order_by('name');
		$query = $this->db->get('job_cities');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_jobs_currencies(){
		$this->db->where(array("status" => '1'));
		$this->db->order_by('ordering');
		$query = $this->db->get('job_currencies');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_jobs_salaryrange(){
		$this->db->where(array("status" => '1'));
		$this->db->order_by('ordering');
		$query = $this->db->get('job_salaryrange');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_jobs_salaryrangetypes(){
		$this->db->where(array("status" => '1'));
		$this->db->order_by('ordering');
		$query = $this->db->get('job_salaryrangetypes');
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_recent_jobs(){
		$this->db->select('jobs.job_title, jobs.start_publishing, job_companies.name as company_name, job_departments.name as department_name, job_categories.cat_title as category_name, job_subcategories.title as subcategory_name, job_jobtypes.title as job_type');
		$this->db->from('jobs');
		$this->db->join('job_companies', 'job_companies.id = jobs.companyid');
		$this->db->join('job_departments', 'job_departments.id = jobs.departmentid', 'LEFT');
		$this->db->join('job_categories', 'job_categories.id = jobs.categoryid', 'LEFT');
		$this->db->join('job_subcategories', 'job_subcategories.id = jobs.subcategoryid', 'LEFT');
		$this->db->join('job_jobtypes', 'job_jobtypes.id = jobs.job_type', 'LEFT');
		$this->db->where("jobs.start_publishing between DATE_SUB(NOW(), INTERVAL 7 DAY) and NOW()" );
		
		$query = $this->db->get();
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	function get_applied_jobs($resume_id){
		$this->db->select('jobs.job_title, jobs.start_publishing, job_companies.name as company_name, job_jobtypes.title as job_type, jobs_resumes.applied_date');
		$this->db->from('jobs');
		$this->db->join('job_companies', 'job_companies.id = jobs.companyid');
		$this->db->join('job_jobtypes', 'job_jobtypes.id = jobs.job_type', 'LEFT');
		$this->db->join('jobs_resumes', 'jobs_resumes.job_id = jobs.jobs_id');
		$this->db->where(array("jobs_resumes.resume_id" => $resume_id));
		$query = $this->db->get();
		//echo $str = $this->db->last_query();
		return $query->result();
	}
	
	/***
	 * End CV DATABASE Jobs portal
	 */
	
	/**
	 * Help Service Desk Block
	 * @param unknown $file_data
	 * @param string $formId
	 */
	function get_itmanger(){
		//$query = "SELECT EMP_NAME from emp_basic_info where emp_num='11252'";
		$where_clause  = " where users.department_id=110 ";
	
		$query = "SELECT users.user_id, users.name from users join userrole on users.user_id = userrole.user_id  $where_clause and role_id IN (10, 11) order by role_id desc";
		$query = $this->db->query($query);
		$result = $query->row();
		return $result->name;
	}
	function addWorkflowLog($formId, $formType,$subType,$emp_num,$status,$ownerId,$url, $comments=null)
	{
		if(!isset($formType)){
			$formType = $this->formType;
		}
	
		if(!isset($url)){
			$url = $this->formUrl."&formId=".$formId."&formType=".$formType."&subType=".$subType."&emp_num=".$ownerId;
		}
		$updated = NULL;
		if(!is_null($comments)){ $updated = 'NOW()';}
	
		$query = " insert into workflow_log (form_id, form_type, sub_type, emp_num, status, form_owner_id,form_url,comments,updated_date) values(".$formId.",'".$formType."','".$subType."',".$emp_num.",'".$status."',".$ownerId.",'".$url."', '".$comments."', '$updated')";
	
		$this->db->query($query);
	}
	
	function addFormLog($formId, $formType,$subType,$emp_num,$status,$formUrl,$to)
	{
		$readOnlyUrl = addslashes($this->formReadOnlyUrl."&formId=".$formId."&emp_num=".$emp_num);
		$company_id = $this->session->userdata (SESSION_CONST_PRE.'company_id');
		$branch_id  = $this->session->userdata (SESSION_CONST_PRE.'branch_id') ;
	
		$query = "insert into form_logs (form_id, form_type, sub_type, emp_num, status,form_url,assign_to_id,readonly_url, branch_id, company_id, subject, priority, urgency) values(".$formId.",'".$formType."','".$subType."',".$emp_num.",'".$status."','".$formUrl."','".$to."','".$readOnlyUrl."', '".$branch_id."', '".$company_id."', '".$this->subject."', '".$this->priority."', '".$this->urgency."')";
	
		$this->db->query($query);
		$id = $this->db->insert_id();
		$this->updateFormRefNumber($id);
		return $id;
	}
	
	function updateFormRefNumber($log_id)
	{
		$company_id = $this->session->userdata(SESSION_CONST_PRE.'company_id');
		$branch_id = $this->session->userdata(SESSION_CONST_PRE.'branch_id');
		$this->ref = Util::generateFormRefNumber($log_id,$this->formType, $company_id);
		
		$query = "update form_logs set ref_number = '".$this->ref."' where form_logs_id='".$log_id."'";
		$this->db->query($query);
	}
	
	function setPriorityUrgency($formId, $formType){
	
		$query = "Update form_logs SET priority = '".$this->priority."', urgency = '".$this->urgency."' where form_id='".$formId."' and form_type='".$formType."'";
		$query = $this->db->query($query);
	}
	function getFormLogDetail($formId,$formType)
	{
		$query = "select * from form_logs where form_id='".$formId."' and form_type='".$formType."'";
		$query = $this->db->query($query);
		$result = $query->result();
		return $result[0];
	}
	
	function getWorkflowLogId($formId,$formType)
	{
		$emp_num = $this->session->userdata(SESSION_CONST_PRE.'userId');
		$query = "select workflow_log_id as id from workflow_log where form_id='".$formId."' and form_type='".$formType."' and emp_num='".$emp_num."' order by workflow_log_id desc";
		$query = $this->db->query($query);
		$result = $query->result();
		if (sizeof($result) > 0)
			return $result[0]->id;
	}
	
	function getFormLevel($formId)
	{
		$query = "select level from form_logs where form_id='".$formId."' and form_type='".$this->formType."'";
		$query = $this->db->query($query);
		$result = $query->result();
		return $result[0]->level;
	}
	
	function getBasicInfo($emp_num)
	{
		$query = "SELECT COMP_CODE, COMP_NAME, substring(EMP_NUM,2) as EMP_NUM, EMP_NAME, DEP_CODE, DEP_NAME, GRADE_CODE, GRADE_LEVEL, BRANCH_CODE, BRANCH_NAME, DESIG_CODE, DESIG_DESC, EMAIL_ID, username, password, manager_id, admin_exe_id, admin_manager_id, account_id, branch_manager_id, director_id, cfo_id, md_id, cashier_id, leave_balance, ticket_entitlement, leave_year_from, leave_year_to, leave_year_ent_days, terms_annual_leave, date_last_annual_leave_from, date_last_annual_leave_to, date_last_annual_leave_days, bal_air_ticket, ere, gm_id, ticket_balance, ticket_route, term_of_annual_leave, term_of_annual_ticket, cost_center, emp_role, emp_salary, asoff_date, account_number from emp_basic_info where emp_num='".$emp_num."'";
		$query = $this->db->query($query);
		return $query->result();
	}
	
	function getUploadedFiles($formLogId){
		$where_clause = '';
		if(!is_null($this->formType)){
			$where_clause = " and form_type='".$this->formType."'";
		}
		$query = "select *, DATE_FORMAT(n.created_date,'%b %d, %Y %h:%i %p') as date from upload_file_log n join users u on n.uploaded_by = u.user_id where n.form_log_id='".$formLogId."'". $where_clause;
		$query = $this->db->query($query);
		return $query->result();
	}
	
	function getRequestNotes($formId, $formType){
		$where = "";
		$curr  = $this->session->userdata(SESSION_CONST_PRE.'userId');
		$owner = $this->owner_id;
		if($owner == $curr) $where = "and n.access = 'Public'";
		$query = "select *, DATE_FORMAT(n.created_date,'%b %d, %Y %h:%i %p') as date from form_notes n
		          join users u on n.user_id = u.user_id
		          where n.form_id='".$formId."'
		          and n.form_type='".$formType."' $where ";
		$query = $this->db->query($query);
		return $query->result();
	}
	
	function getWorkflowLogs($formId, $formType){
		$query = "select *,DATE_FORMAT(wf.created_date,'%b %d, %Y %h:%i %p') as cdate, DATE_FORMAT(wf.updated_date,'%b %d, %Y %h:%i %p') as date from workflow_log wf join users u on wf.emp_num = u.user_id where wf.form_id='".$formId."' and wf.form_type='".$formType."' order by workflow_log_id";
		$query = $this->db->query($query);
		return $query->result();
	}
	
	function getInventoryCategory($val=0){
		$this->db->order_by('inventory_id');
		$this->db->where('department_id', $val);
		//$this->db->where('type', '1');
		$query = $this->db->get('inventory');
		return $query->result();
	}
	
	function getCategoryList($type = 'IT'){
		$query = "select * from category where service_template = '$type' order by category_id ";
		$query = $this->db->query($query);
		return $query->result();
	}
	
	function getSubcategoryList($type = 'IT'){
		if($type == 'IT'){
			$query = "select * from subcategory where category_id <= 1000";
		}
			elseif($type == 'TSD'){
					$query = "select * from subcategory where category_id > 1000 AND category_id <=2000"; // For TSD
					}
					elseif($type == 'Admin'){
						$query = "select * from subcategory where category_id > 2000 AND category_id <=3000"; // For Admin
		}
		elseif($type == 'Facilities'){
			$query = "select * from subcategory where category_id > 3000 AND category_id <=4000"; // For Facilities
		}
		elseif(is_numeric($type)){
			$query = "select * from subcategory where category_id =$type"; // For TSD
		}

		$query = $this->db->query($query);
			return $query->result();
	}
	
			
	function getAllTechnician($flag, $assign_list)
	{
		$branch_id = $this->session->userdata(SESSION_CONST_PRE.'branch_id');
		$where_clause = '';
		if(empty($assign_list)){
			$assign_list = '0';
		}
	
		if($flag){
		$where_clause = " and users.user_id not in ($assign_list)";
		}
		else
		{
			$where_clause = " and users.user_id in ($assign_list)";
		}
	
		$where_clause .= " and users.department_id in (111, 112)";
		$where_clause .= " and users.branch_id = ".$branch_id;
	
		$query = "SELECT users.user_id, users.name from users join userrole on users.user_id = userrole.user_id and role_id = 5  $where_clause";
		$query = $this->db->query($query);
		return $query->result();
	}
	
	
	function getITDirectorID()
	{
		$query = "SELECT user_id, name from users where user_id = '365'";
		$query = $this->db->query($query);
		return $query->result();
	}
	
	function get_itmanager_empnum(){
		$branch_id = $this->session->userdata(SESSION_CONST_PRE.'branch_id');
		$where_clause  = " where emp_basic_info.DEP_CODE='IT'";
		$where_clause .= " and emp_basic_info.BRANCH_CODE=10"; //.$branch_id;
	
		$query = "SELECT EMP_NUM as user_id, EMP_NAME as name from emp_basic_info $where_clause ";
		$query = $this->db->query($query);
		$result = $query->row();
		return $result->user_id;
	}
	
	function getVendorList(){
		$query = $this->db->get('vendors');
		return $query->result();
	}
	
	function getBrandsList(){
		$query = $this->db->get('brands');
		return $query->result();
	}
	
	function getHardwareList(){
		$query = $this->db->get('hardware');
		return $query->result();
	}
	function getServicesList(){
		$query = $this->db->get('services');
		return $query->result();
	}
				
	/**
	 * End Help Service Desk Block
	 */	
	
	/**
	 * POS Block
	 * Enter description here ...
	 * @param $file_data
	 * @param $formId
	 */
	
	function get_all_branchs($b = null){
		if(!is_null($b)){
			$sql = "SELECT * FROM branchs
                    WHERE name LIKE ? ";
			$this->db->order_by('name');
        	$query = $this->db->query($sql, array("$b%"));
		}
		else 
		{
			$this->db->order_by('name');
			$query = $this->db->get('branchs');
		}
        return $query->result();
	}
	
	function get_all_category(){
		$query = $this->db->get('category');
		return $query->result();
	}
	
	function get_units(){
		
		$query = $this->db->get('units');
		return $query->result();
	}
	
	function get_inventory($t = null){
		if(!is_null($t)){
			$this->db->where("type", $t);
		}
		$this->db->order_by('name');
        $query = $this->db->get('inventory');
        return $query->result();
	}
	
	function get_inventory_by($supplier = null){
		if(!is_null($supplier)){
			$this->db->where("vendor_id", $supplier);
		}
		$this->db->where("type", "1");
		$this->db->order_by('name');
        $query = $this->db->get('inventory');
        return $query->result();
	}
	
	function get_eod(){
		/*$this->db->select('inv.inventory_id, inv.name');
		$this->db->from('inventory inv');
		$this->db->join('end_of_day eod','inv.inventory_id = eod.inventory_id', 'LEFT');
		$query = $this->db->get();
		return $query->result();*/
		$this->db->where("type", "1");
		$this->db->order_by('name');
        $query = $this->db->get('inventory');
        return $query->result();
	}
	
	function last_eod_exist(){
		$this->db->select('COUNT(inventory_id) as num')->from('end_of_day');
		$this->db->where("eod_date = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY),'%Y-%m-%d')" );
		$query = $this->db->get();
        $rs = $query->result();
        if($rs[0]->num == 0) // no last eod exist
        {
        	$query = "SELECT DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY),'%Y-%m-%d') as last_date";
			$rs   = $this->db->query($query);
			$result = $rs->result();
			return $result[0]->last_date; 
        }
        else
        return date('Y-m-d');
	}
	function getLastDay(){
		$query = "SELECT DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY),'%Y-%m-%d') as last_date";
		$rs   = $this->db->query($query);
		$result = $rs->result();
		return $result[0]->last_date; 
	}
	function getInventoryList($t = null){
		if(!is_null($t)){
			$this->db->where("type", $t);
		}
		$this->db->order_by('name');
        $query = $this->db->get('inventory');
        return $query->result();
	}

	function getRentalInventoryList(){
		$this->db->order_by('name');
        $query = $this->db->get('rental_inventory');
        return $query->result();
	}

	function getPreorderInventoryList(){
		$this->db->order_by('name');
        $query = $this->db->get_where('inventory', array('type'=>'9'));
        return $query->result();
	}
	
	function get_today_sale(){
		$today = date('Y-m-d');
		
		/*$this->db->select('SUM(quantity*unit_price) as total_sale');
		$this->db->from('receipt');
		$this->db->where("returned_qty", '0');
		$this->db->where("created_date LIKE '$today%'" );*/
		
		$this->db->select('SUM(total_amount) as total_sale')->from('order');
		$this->db->where("total_amount >", 0);
		$this->db->where("created_date LIKE '$today%'" );
		$this->db->order_by('order_id', 'DESC');
		
		$query = $this->db->get();  //$str = $this->db->last_query(); echo '<br/><br/><br/><br/><br/><br/>'.$str;
		$result = $query->result();
		
		return $result[0]->total_sale;
	}
	
	function get_adv_orders(){
		$next_day	   = date('Y-m-d', strtotime("+1 day"));
		$this->db->select('COUNT(inventory_id) as total_count');
		$this->db->from('receipt');
		$this->db->where("start_date LIKE '$next_day%'" );
		$query = $this->db->get(); 
		$result = $query->result();
		
		return $result[0]->total_count;
	}
	
	function get_today_returns(){
		$today	   = date('Y-m-d');
		$this->db->select('COUNT(receipt_id) as total_count');
		$this->db->from('credit_note');
		$this->db->where("created_date LIKE '$today%'" );
		$query = $this->db->get(); 
		$result = $query->result();

		return $result[0]->total_count;
	}
	
	function get_inventory_count(){
		$this->db->select('COUNT(inventory_id) as total_count');
		$this->db->from('inventory');
		$this->db->where('type >', '1');
		$query = $this->db->get(); 
		$result = $query->result();
		
		return $result[0]->total_count;
	}

	function getRecentSaleList(){
		$this->db->where("total_amount >", 0);
		$this->db->order_by('order_id', 'DESC');
        $query = $this->db->get('order', '10');
        return $query->result();
	}
	
	function getInventoryBestSellingItems(){
			
		$this->db->order_by('hits', 'DESC');
        $query = $this->db->get('inventory', '10');
        return $query->result();
	}
	function getCompanyList(){
		
		if($this->data['userrole'] != '1'){
			$company_id = $this->session->userdata(SESSION_CONST_PRE.'company_id'); 
			$this->db->where(array("company_id" => $company_id));
		}
		$this->db->order_by('name');
        $query = $this->db->get('company');
        return $query->result();
	}
	
	function getBranchListByCompany($id=null){
		if(is_null($id)){
			$id = $this->session->userdata(SESSION_CONST_PRE.'company_id');
		} 

		$this->db->where(array("company_id" => $id));
		$this->db->order_by('name');
        $query = $this->db->get('branchs');
        return $query->result();
	}
	
	function getRecipeList($current_recipe = null){
		if(!is_null($current_recipe)){
			$this->db->where_not_in("recipe_id","$current_recipe");
		}
		$this->db->order_by('name');
		$query = $this->db->get('recipe');
        return $query->result();
	}

	public function get_receipt_id(){
		$query = $this->db->query("select max(receipt_id) as id from receipt");
		$result = $query->result();
		return $result[0]->id + 1;
			
	}
		
	/**
	 * END POS Block
	 */
	
	function upload_file_log($file_data, $formId = null){
		$branch = '';
		if(isset($_COOKIE[SESSION_CONST_PRE.'branch']))
		{
			$branch = $_COOKIE[SESSION_CONST_PRE.'branch'];
		}
		
		$form = array('form_log_id'=>$formId,'form_type'=>$this->formType, 'branch' => $branch, 'uploaded_by' => $this->session->userdata(SESSION_CONST_PRE.'userId') );
		$arr_upload = array_merge($file_data, $form);
		$this->db->insert('upload_file_log', $arr_upload);
	}
	
	function insert_lables($lbl, $text){
		$query = "select localize_id from localize where localize_id='$lbl'";
		$query = $this->db->query($query);
		$rs = $query->result();
	
		if(!isset($rs[0])){
				
			$this->db->insert('localize', array('localize_id'=>$lbl, 'lang_en'=>$text));
		}
	}

	public function leading_zeros($value, $places){
	 	$leading="";
	    if(is_numeric($value)){
	        for($x = 1; $x <= $places; $x++){
	            $ceiling = pow(10, $x);
	            if($value < $ceiling){
	                $zeros = $places - $x;
	                for($y = 1; $y <= $zeros; $y++){
	                    $leading .= "0";
	                }
	            $x = $places + 1;
	            }
	        }
	        $output = $leading . $value;
	    }
	    else{
	        $output = $value;
	    }
	    return $output;
	}
}
