<?php

$lang['en_invalid_connection_str'] = 'Unable to determine the database settings based on the connection string you submitted.';
$lang['en_unable_to_connect'] = 'Unable to connect to your database server using the provided settings.';
$lang['en_unable_to_select'] = 'Unable to select the specified database: %s';
$lang['en_unable_to_create'] = 'Unable to create the specified database: %s';
$lang['en_invalid_query'] = 'The query you submitted is not valid.';
$lang['en_must_set_table'] = 'You must set the database table to be used with your query.';
$lang['en_must_use_set'] = 'You must use the "set" method to update an entry.';
$lang['en_must_use_index'] = 'You must specify an index to match on for batch updates.';
$lang['en_batch_missing_index'] = 'One or more rows submitted for batch updating is missing the specified index.';
$lang['en_must_use_where'] = 'Updates are not allowed unless they contain a "where" clause.';
$lang['en_del_must_use_where'] = 'Deletes are not allowed unless they contain a "where" or "like" clause.';
$lang['en_field_param_missing'] = 'To fetch fields requires the name of the table as a parameter.';
$lang['en_unsupported_function'] = 'This feature is not available for the database you are using.';
$lang['en_transaction_failure'] = 'Transaction failure: Rollback performed.';
$lang['en_unable_to_drop'] = 'Unable to drop the specified database.';
$lang['en_unsuported_feature'] = 'Unsupported feature of the database platform you are using.';
$lang['en_unsuported_compression'] = 'The file compression format you chose is not supported by your server.';
$lang['en_filepath_error'] = 'Unable to write data to the file path you have submitted.';
$lang['en_invalid_cache_path'] = 'The cache path you submitted is not valid or writable.';
$lang['en_table_name_required'] = 'A table name is required for that operation.';
$lang['en_column_name_required'] = 'A column name is required for that operation.';
$lang['en_column_definition_required'] = 'A column definition is required for that operation.';
$lang['en_unable_to_set_charset'] = 'Unable to set client connection character set: %s';
$lang['en_error_heading'] = 'A Database Error Occurred';

/* End of file en_lang.php */
/* Location: ./system/language/english/en_lang.php */