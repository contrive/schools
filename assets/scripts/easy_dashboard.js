var Charts = function () {

    return {
        //main function to initiate the module

        init: function () {

            App.addResponsiveHandler(function () {
                 Charts.initPieCharts(); 
            });
            
        },

        initBarCharts: function () {

            // bar chart:
            var data1 = GenerateSeries(0);
     
            function GenerateSeries(added){
                var data = [];
                var start = 100 + added;
                var end = 200 + added;
         
                for(i=1;i<=20;i++){        
                    var d = Math.floor(Math.random() * (end - start + 1) + start);        
                    data.push([i, d]);
                    start++;
                    end++;
                }
         
                return data;
            }
         
            var options = {
                    series:{
                        bars:{show: true}
                    },
                    bars:{
                          barWidth:0.8
                    },            
                    grid:{
                        backgroundColor: { colors: ["#fafafa", "#35aa47"] }
                    }
            };
 
            $.plot($("#chart_1_1"), [data1], options);             
        },

        initPieCharts: function () {

            var data = [];
            var series = Math.floor(Math.random() * 10) + 1;
            series = series < 5 ? 5 : series;
            
            for (var i = 0; i < series; i++) {
                data[i] = {
                    label: "Requests" + (i + 1),
                    data: Math.floor(Math.random() * 100) + 1
                };
            }

            // INTERACTIVE
            $.plot($("#interactive"), data, {
                    series: {
                        pie: {
                            show: true
                        }
                    },
                    grid: {
                        hoverable: true,
                        clickable: true
                    }
                });
            $("#interactive").bind("plothover", pieHover);
            $("#interactive").bind("plotclick", pieClick);

            function pieHover(event, pos, obj) {
            if (!obj)
                    return;
                percent = parseFloat(obj.series.percent).toFixed(2);
                $("#hover").html('<span style="font-weight: bold; color: ' + obj.series.color + '">' + obj.series.label + ' (' + percent + '%)</span>');
            }

            function pieClick(event, pos, obj) {
                if (!obj)
                    return;
                percent = parseFloat(obj.series.percent).toFixed(2);
                alert('' + obj.series.label + ': ' + percent + '%');
            }

        }
        
    };

}();