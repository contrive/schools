<?php		
Class Util
{	
	function number_pad($number,$n) {
		return str_pad((int) $number,$n,"0",STR_PAD_LEFT);
	}
	
	function ToggleLang($label){		
		return $label;
	}
	
	function view_date($dt){		
		return substr($dt,8,2).'-'.substr($dt,5,2).'-'.substr($dt,0,4);
	}
}
?>